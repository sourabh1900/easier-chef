import React from "react";
import {Grid} from "@material-ui/core";
import Card from "./Card/Card";

const cards = (props) => {
    console.log(props.recipes)
    return (
        <React.Fragment>
            {props.recipes && props.recipes.length > 0 ? (
                props.recipes.map((recipe) => {
                    const readyInTime = parseInt(recipe.cookingTime) + parseInt(recipe.preparationTime) + "min";
                
                    return (
                        <Grid item key={recipe.id}>
                            <Card
                                recipeName={recipe.name}
                                recipeReadyInTime={readyInTime}
                                makeItBtn={props.makeItBtn}
                                deleteBtn={props.deleteBtn}
                                id={recipe.id}
                                picture={recipe.picture}
                            />
                        </Grid>
                    );
                })
            ) : (
                <h3>No Recipe found</h3>
            )}
        </React.Fragment>
    );
};

export default cards;
