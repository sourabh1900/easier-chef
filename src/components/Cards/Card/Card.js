import React from "react";
import {
    Card,
    CardActionArea,
    CardMedia,
    Typography,
    CardActions,
    Button,
    IconButton,
    Grid, CardHeader,
} from "@material-ui/core";
import {Timer} from "@material-ui/icons";
import classes from "./Card.module.css";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

const card = (props) => {
    return (
        <Card className={classes.Card}>
            <CardHeader
                action={
                    <IconButton aria-label="remove" >
                        <DeleteOutlineIcon id={props.id} onClick={props.deleteBtn}  />
                    </IconButton>
                }
                title= {props.recipeName}
            />
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="recipe"
                    height="140"
                    image={props.picture}
                />
            </CardActionArea>
            <CardActions>
                <Grid container justify="space-between">
                    <Grid item xs={4}>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={props.makeItBtn}
                            id={props.id}
                        >
                            View
                        </Button>
                    </Grid>

                    <Grid item container xs={8} justify="flex-end" alignItems="center">
                        <IconButton color="primary">
                            <Timer/>
                        </IconButton>
                        <Typography variant="h6">{props.recipeReadyInTime}</Typography>
                    </Grid>
                </Grid>
            </CardActions>
        </Card>
    );
};

export default card;
