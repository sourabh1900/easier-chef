import React from "react";
import Header from "../Header/Header";
import AddRecipe from "../AddRecipe/AddRecipe";

const layout = (props) => {
    return (
        <React.Fragment>
            <Header searchBox={props.searchBox}/>
            {props.children}
            <AddRecipe />
        </React.Fragment>
    );
};

export default layout;
