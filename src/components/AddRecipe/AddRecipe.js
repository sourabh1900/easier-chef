import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import classes from "./AddRecipe.module.css";
import { Link } from 'react-router-dom';

const addRecipe = () => {

    return (
        <div className={classes.AddButton}>
            <Fab color="primary" aria-label="add" component={Link} to="/create">
                <AddIcon/>
            </Fab>
        </div>
    );
};

export default addRecipe;
