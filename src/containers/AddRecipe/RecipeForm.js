import React, {Component} from 'react';
import {connect} from 'react-redux';
import FormError from "../../components/FormError";
import {updateRecipeFormData} from '../../store/actions/recipeForm';
import {createRecipe} from '../../store/actions/recipeActions';
import classes from "./RecipeForm.module.css";
import {Grid} from "@material-ui/core";
import Button from '@material-ui/core/Button';

class RecipeForm extends Component {

    handleOnChange = event => {
        const {name, value} = event.target;
        const currentRecipeFormData = Object.assign({}, this.props.recipeFormData, {
            [name]: value
        })
        console.log(currentRecipeFormData)
        this.props.updateRecipeFormData(currentRecipeFormData)
    }

    handleOnSubmit = e => {
        e.preventDefault();
        const {createRecipe, recipeFormData, history} = this.props;
        if(recipeFormData.instructions) {
            recipeFormData.instructions = recipeFormData.instructions.split('\n');
        }
       createRecipe(recipeFormData, history);
    }

    // handle input change
    handleInputChange = (e, index) => {
        const {name, value} = e.target;
        const ingredients = [...this.props.recipeFormData.ingredients];
        ingredients[index][name] = value;
        const currentRecipeFormData = Object.assign({}, this.props.recipeFormData, {
            ingredients: ingredients
        })
        this.props.updateRecipeFormData(currentRecipeFormData)
    };

    // handle click event of the Remove button
    handleRemoveClick = index => {
        console.log(index);
        const ingredients = [...this.props.recipeFormData.ingredients];
        ingredients.splice(index, 1);
        const currentRecipeFormData = Object.assign({}, this.props.recipeFormData, {
            ingredients: ingredients
        })
        this.props.updateRecipeFormData(currentRecipeFormData)
    };

    // handle click event of the Add button
    handleAddClick = (list) => {
        console.log(list);
        const ingredients = [...list, {ingredientName: "", ingredientQuantity: 0}];
        const currentRecipeFormData = Object.assign({}, this.props.recipeFormData, {
            ingredients: ingredients
        })
        this.props.updateRecipeFormData(currentRecipeFormData)
    };

    render() {
        const {name, ingredients, instructions, cookingTime, preparationTime, picture} = this.props.recipeFormData;
        return (
            <Grid container className={classes.Recipe} direction="column">
                <Grid item xs={12}>
                    <h2>Create a New Recipe</h2>
                </Grid>
                <Grid item container xs={12}>
                    {this.props.errors === true ? <FormError/> : null}

                    <form onSubmit={this.handleOnSubmit}>
                        <div>
                            <label htmlFor="name">Name:</label>
                            <input
                                className={classes.inputText}
                                type="text"
                                onChange={this.handleOnChange}
                                name="name"
                                value={name}
                            />
                        </div>

                        <div>
                            <label htmlFor="ingredients">Ingredients:</label>
                            {ingredients.map((x, i) => {
                                return (
                                    <Grid item xs={12} key={i}>
                                        <Grid item xs={6}>
                                            <input
                                                className={classes.inputText}
                                                type="text"
                                                placeholder="Enter Ingredient Name"
                                                onChange={e => this.handleInputChange(e, i)}
                                                name="ingredientName"
                                                value={x.ingredientName}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <input
                                                placeholder="Enter Quantity of Ingredient"
                                                className={classes.inputText}
                                                type="text"
                                                onChange={e => this.handleInputChange(e, i)}
                                                name="ingredientQuantity"
                                                value={x.ingredientQuantity}
                                            />
                                        </Grid>
                                        <Grid item xs={2}>
                                            {ingredients.length !== 1 &&
                                            <Button onClick={() => this.handleRemoveClick(i)} variant="outlined"
                                                    color="secondary">Remove</Button>}
                                            {ingredients.length - 1 === i &&
                                            <Button onClick={() => this.handleAddClick(ingredients)} variant="outlined"
                                                    color="primary">Add</Button>}
                                        </Grid>
                                    </Grid>
                                );
                            })}
                        </div>

                        <div>
                            <label htmlFor="instructions">Instructions:</label>
                            <textarea
                                onChange={this.handleOnChange}
                                name="instructions"
                                value={instructions}
                            />
                        </div>
                        <div>
                            <label htmlFor="picture">Image URL:</label>
                            <input
                                className={classes.inputText}
                                type="url"
                                onChange={this.handleOnChange}
                                name="picture"
                                value={picture}
                            />
                        </div>
                        <div>
                            <label htmlFor="cookingTime">Cook Time:</label>
                            <input
                                className={classes.inputText}
                                type="text"
                                onChange={this.handleOnChange}
                                name="cookingTime"
                                value={cookingTime}
                            />
                        </div>
                        <div>
                            <label htmlFor="preparationTime">Preparation Time:</label>
                            <input
                                className={classes.inputText}
                                type="text"
                                onChange={this.handleOnChange}
                                name="preparationTime"
                                value={preparationTime}
                            />

                            <Button type={"submit"} variant="contained" color="primary">Add Recipe</Button>
                        </div>
                    </form>
                </Grid>
            </Grid>
        )
    }
}

const mapStateToProps = state => {
    return {
        recipeFormData: state.recipeFormData,
        errors: state.errors
    }
}

export default connect(mapStateToProps, {
    updateRecipeFormData,
    createRecipe
})(RecipeForm);
