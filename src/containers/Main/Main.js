import React, {Component} from "react";
import {Grid, Typography} from "@material-ui/core";
import Cards from "../../components/Cards/Cards";
import classes from "./Main.module.css";
import {connect} from "react-redux";
import {deleteRecipe} from "../../store/actions/recipeActions";

class Main extends Component {
    makeItHandler = (e) => {
        const recipeIdSelected = e.target.parentNode.id || e.target.id;
        if (recipeIdSelected) {
            this.props.history.push("/view/" + recipeIdSelected);
        }
    };

    deleteHandler = (e) => {
        const recipeIdSelected = e.target.id;
        if (recipeIdSelected) {
            const {deleteRecipe, history} = this.props;
            deleteRecipe(recipeIdSelected, history)
        }
    }

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                className={classes.MainContent}
            >
                <Grid container justify="center">
                    <Typography variant="h3">Welcome to the Easier Chef!</Typography>
                </Grid>

                <Grid item container xs={12} justify="space-evenly">
                    <Cards
                        recipes={
                            this.props.searchResults
                                ? this.props.searchResults
                                : this.props.recipes
                        }
                        makeItBtn={(e) => this.makeItHandler(e)}
                        deleteBtn={(e) => this.deleteHandler(e)}
                    />
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        recipes: state.recipes.recipes,
        searchResults: state.recipes.searchResult,
    };
};

export default connect(mapStateToProps, {deleteRecipe})(Main);
