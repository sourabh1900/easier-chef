import * as actionTypes from "./actionTypes";
export const updateRecipeFormData = recipeFormData => {
  return {
    type: actionTypes.UPDATED_RECIPE,
    recipeFormData
  }
}
export const resetRecipeForm = () => {
  return {
    type: actionTypes.RESET_RECIPE_FORM
  }
}
