import {resetRecipeForm} from './recipeForm';
import {guidGenerator} from "../../helper";
import * as actionTypes from "./actionTypes";

export const setRecipes = (recipes) => {
    return {
        type: actionTypes.ALL_RECIPES,
        recipes
    }
}

export const addRecipe = recipe => {
    return {
        type: actionTypes.CREATE_RECIPE,
        recipe
    }
}

export const removeRecipe = recipeId => {
    return {
        type: actionTypes.REMOVE_RECIPE,
        recipeId
    }
}


export const createRecipe = (recipe, routerHistory) => {
    return dispatch => {
        let recipes = JSON.parse(localStorage.getItem('recipes')) || [];
        let newId = guidGenerator();
        recipes.push({...recipe, id: newId});
        localStorage.setItem('recipes', JSON.stringify(recipes));
        dispatch(addRecipe(recipe))
        dispatch(resetRecipeForm())
        dispatch(getAllRecipes());
    }
}

export const deleteRecipe = (recipeId, routerHistory) => {
    return dispatch => {
        let recipes = JSON.parse(localStorage.getItem('recipes'));
        if (recipes === null) {
            return;
        }

        for (var i = 0; i < recipes.length; i++) {
            if (recipes[i].id === recipeId) {
                recipes.splice(i, 1);
                break;
            }
        }
        localStorage.setItem('recipes', JSON.stringify(recipes));
        dispatch(removeRecipe(recipeId));
        dispatch(getAllRecipes());
    }
}

export function getAllRecipes() {
  return dispatch => {
    let recipes = JSON.parse(localStorage.getItem('recipes')) || [];
    dispatch(setRecipes({
        recipes: recipes
    }));
  }
}

