import * as actionTypes from "../actions/actionTypes";

const initialState = {
    id: '',
    name: '',
    ingredients: [
        {
            ingredientName: '',
            ingredientQuantity: ''
        }
    ],
    instructions: [],
    preparationTime: 0,
    cookingTime: 0,
    picture: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATED_RECIPE:
            return action.recipeFormData

        case actionTypes.RESET_RECIPE_FORM:
            return initialState;

        default:
            return state;
    }
}
