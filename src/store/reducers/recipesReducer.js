import * as actionTypes from "../actions/actionTypes";

const initialState = {
    recipes: localStorage.getItem('recipes') ? JSON.parse(localStorage.getItem('recipes')) : [
        {
            id: '1234-dummy-id-6789',
            name: "Cake",
            picture: "https://spoonacular.com/recipeImages/579247-556x370.jpg",
            preparationTime: 20,
            cookingTime: 30,
            instructions: [
                "Prepare Baking Pans",
                "Allow Ingredients to Reach Room Temperature"
            ],
            ingredients: [
                {
                    ingredientName: "Baking Powder",
                    ingredientQuantity: "10gm",
                },
                {
                    ingredientName: "Butter",
                    ingredientQuantity: "100gr",
                },
                {
                    ingredientName: "Eggs",
                    ingredientQuantity: "3",
                },
            ],
        }
    ],
    search: "",
    searchResult: null,
};

const reducer = (state = initialState, action) => {
    console.log('action...', action)
    console.log('state...', state)
    switch (action.type) {
        case actionTypes.SEARCH_SUCCESS:
            return {
                ...state,
                search: action.value,
            };
        case actionTypes.SEARCH_RESULT_SUCCESS:
            return {
                ...state,
                searchResult: action.results,
            };
        case actionTypes.CLEAR_SEARCH_RESULTS:
            return {
                ...state,
                searchResult: null,
            };
        case actionTypes.ALL_RECIPES:
            return {
                ...state,
                searchResult: null,
            };

        case actionTypes.CREATE_RECIPE:
            return  {
              ...state,
              recipes:  state.recipes.concat(action.recipe)
            };

        case actionTypes.REMOVE_RECIPE:
            return {
              ...state,
              recipes: state.recipes.filter(recipe => recipe.id !== action.recipeId)
            };

        default:
            return state;
    }
};

export default reducer;
